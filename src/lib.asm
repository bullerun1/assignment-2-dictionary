
section .data
%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define READ_SYSCALL 0
%define STDERR 2
%define STDOUT 1
%define STDIN 0
%define NULL_TERMINANT 0
%define DIVIDE_TEN 10
%define LENGHT_OF_A_TWENTY_BIT_NUMBER 20
buffer db 1           ; Буфер для считывания одного байта
new_line:      db  `\n`, NULL_TERMINANT
section .text

global exit
global string_length
global print_string
global string_copy
global string_equals
global print_char
global print_int 
global print_uint
global print_newline
global parse_int
global parse_uint
global read_char
global read_string
global print_error_string

; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
   .counter:
    cmp  byte [rdi+rax], NULL_TERMINANT
    je   .end
    inc  rax
    jmp  .counter
  .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax
    mov  rax, WRITE_SYSCALL
    mov  rdi, STDOUT
    syscall
    ret

print_error_string:
    push rdi
    call string_length
    pop  rsi
    mov  rdx, rax
    mov  rax, WRITE_SYSCALL
    mov  rdi, STDERR
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
     mov   rdi, new_line     ; string address


; Принимает код символа и выводит его в stdout
print_char:
     push rdi
     mov     rsi, rsp     ; string address
     mov     rax, WRITE_SYSCALL          ; 'write' syscall number
     mov     rdi, STDOUT           ; stdout descriptor
     mov     rdx, 1          ; string length in bytes
     syscall
     pop rdi
     ret

     
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi ; перемещаем данные в аккамулятор
    sub rsp, 24  ; резервируем стэк под 20 символов + нуль терминант
    mov rcx, LENGHT_OF_A_TWENTY_BIT_NUMBER ; всего 8-ми байтовое число может иметь 20 символов
    mov r8, DIVIDE_TEN ; используем дополнительный регистр как делитель
    mov byte[rsp + LENGHT_OF_A_TWENTY_BIT_NUMBER], NULL_TERMINANT ;загружаем нуль терминант
.loop:
    dec rcx ; декриментим указатель
    xor rdx, rdx ; очищаем остаток от деления
    div r8 ; делим
    add dl, '0' ; dl-младший полубайт rdx, прибавляем код 0 из ASCII
    mov byte[rcx+rsp], dl ; записываем данные с помощью косвенной адресации
    test rax, rax ; ставим флаг
    jnz .loop ;проверка флага, если аккамулятор не ноль то лупаемся

    mov rdi, rsp ;ставим указатель на вершиину стека которую мы задали в самом начало функции
    add rdi, rcx ;сдвигаемся на реальную вершину стека
    call print_string
.exit:
    add rsp, 24 ;возвращаем в изначальное состояние
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jnl .print
.negative:
    push rdi
    mov dil, '-'
    call print_char
    pop rdi
    neg rdi
.print:
    call print_uint
.exit:
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.loop:
    mov r8b, byte[rdi+rax]
    cmp r8b, byte[rsi+rax]
    jnz .false
    test r8b, r8b
    jz .true
    inc rax
    jmp .loop
.true:
    mov rax, 1
    ret
.false:
    xor rax, rax
.exit:
    ret



read_char:
    mov rax, READ_SYSCALL          ; Системный вызов read
    mov rdi, STDIN                 ; Файловый дескриптор stdin (0)
    mov rsi, buffer                ; Указатель на буфер
    mov rdx, 1                     ; Количество байт для чтения
    syscall
    cmp rax, NULL_TERMINANT          ; Проверяем, было ли прочитано 0 байт (конец потока)
    jbe .end
    movzx rax, byte [buffer]  ; Загружаем символ из буфера в RAX
.end:
    ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_string:
    mov r8, rdi
    mov r9, rsi
.checking_withspace_characters:
    push r8
    push r9
    call read_char
    pop r9
    pop r8
    cmp al, ' '
    jz .checking_withspace_characters
    cmp al, `\t`
    jz .checking_withspace_characters
    cmp al, `\n`
    jz .checking_withspace_characters

    xor rcx,rcx
.loop:
    cmp r9, rcx
    jz .error
    mov [r8 + rcx], al
    inc rcx
    test al, al
    jz .correct
    cmp al, `\t`
    jz .correct
    cmp al, `\n`
    jz .correct
    push r8
    push r9
    push rcx
    call read_char
    pop rcx
    pop r9
    pop r8
    jmp .loop
.correct:
    dec rcx
    mov rax, r8
    mov rdx, rcx
    jmp .exit
.error:
    xor rax, rax
    xor rdx, rdx
.exit:
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r10, r10
    xor rax, rax
    mov r9, 10

.loop:
    mov r8b, byte[rdi+r10]
    test r8b, r8b
    jz .exit

    sub r8b, '0'
    cmp r8b, 0
    jb .exit ; проверка на то что в результате вычитания ASCII кода '0' из символа, символ больше или равно 0 как int
    cmp r8b, 9
    ja .exit ; заключительная проверка на то, что после парсинга символа он принадлежит отрезку 0<=x<=9
    mul r9
    add rax, r8
    inc r10
    jmp .loop

.exit:
    mov rdx, r10 
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, '-'
    cmp al, byte[rdi]
    jnz .positive
.negative:
    inc rdi
    call parse_uint
    test rax, rax ; здесь приходит значение числа и проверяется на 0
    jz .exit      ; если оно ноль то
    neg rax
    inc rdx
    ret
.positive:
    call parse_uint
.exit:
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rax, rdx
    jz .error
    mov r8b, byte [rdi + rax]
    mov byte [rsi + rax], r8b
    inc rax
    test r8b, r8b
    jnz .loop
    ret
.error:
    xor rax, rax
.exit:
    ret





; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
.checking_withspace_characters:
    push r8
    push r9
    call read_char
    pop r9
    pop r8
    cmp al, ' '
    jz .checking_withspace_characters
    cmp al, `\t`
    jz .checking_withspace_characters
    cmp al, `\n`
    jz .checking_withspace_characters

    xor rcx,rcx
.loop:
    cmp r9, rcx
    jz .error
    mov [r8 + rcx], al
    inc rcx
    test al, al
    jz .correct
    cmp al, ' '
    jz .correct
    cmp al, `\t`
    jz .correct
    cmp al, `\n`
    jz .correct
    push r8
    push r9
    push rcx
    call read_char
    pop rcx
    pop r9
    pop r8
    jmp .loop
.correct:
    dec rcx
    mov rax, r8
    mov rdx, rcx
    jmp .exit
.error:
    xor rax, rax
    xor rdx, rdx
.exit:
    ret