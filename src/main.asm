%include "words.inc"
%define BUF_SIZE 255

section .rodata

long_word_error: db "the key is too large, enter a value not exceeding 255 characters", 0
key_not_found: db "key not found", 0

section .text
global _start

%include "dict.inc"
%include "lib.inc"
; Читает строку размером не более 255 символов в буфер с stdin.
; Пытается найти вхождение в словаре; если оно найдено, 
; распечатывает в stdout значение по этому ключу. Иначе выдает сообщение об ошибке.
_start:
		mov rdi, rsp
		mov rsi, BUF_SIZE
		call read_string
        test rax, rax
        jz .overflow
        mov rdi, rax
        mov rsi, element
        call find_word
        test rax, rax
		jz .key_error
        lea rdi, [rax+rdx+9]
        call print_string
        xor rdi, rdi
        ret
.key_error:
    mov rdi, key_not_found
    jmp .exit
.overflow:
    mov rdi, long_word_error
.exit:
    call print_error_string
    call exit
