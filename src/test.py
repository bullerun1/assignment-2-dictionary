#!/usr/bin/python3

import subprocess

keys = ['first_word', 'second_word', 'third word', "a", "a" * 256]
output = ['first word explanation', 'second word explanation', 'third word explanation', "", ""]
error = ['', '', '', "key not found", "the key is too large, enter a value not exceeding 255 characters"]

wrong_counter = 0

for i in range(len(keys)):
    process = subprocess.run("./lab2", input=keys[i], text=True, capture_output=True)
    flag = True
    if process.stdout != output[i]:
        print(f"{i + 1} test failed")
        print(f"out:\"{process.stdout}\", expected: {output[i]}")
        wrong_counter += 1
        flag = False

    if process.stderr != error[i]:
        if flag:
            print(f"{i + 1} test failed")
            wrong_counter += 1 
        print(f"out:\"{process.stderr}\", expected {error[i]}")
        flag = False

    if flag:
        print(f"{i + 1} test passed")
if(wrong_counter == 0):
    print("All tests passed")
else:
    print(f"{wrong_counter} tests failed")
