section .text

global find_word

extern string_equals
; Принимает два аргумента:
; Указатель на нуль-терминированную строку. rdi
; Указатель на начало словаря. rsi

; find_word пройдёт по всему словарю в поисках подходящего ключа.
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в
; словарь (не значения), иначе вернёт 0.
find_word:
    xor rax, rax 
    test rsi, rsi
    jz .done
.loop:
    push rdi
    push rsi
    lea rsi, [rsi + 8]
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .done
    mov rsi, [rsi]
    test rsi, rsi
    jnz .loop
    ret
.done:
    mov rax, rsi
    ret
